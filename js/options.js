// Saves options to localStorage.
function save_style_options() {

    var style = {
        'bold': document.getElementById('highlight-bold').checked ? 'bold': 'normal',
        'underline': document.getElementById('highlight-underline').checked ? 'underline': 'none',
        'colorenabled': document.getElementById('highlight-color').checked,
        'color': document.getElementById('highlight-color-picker').value
    };
    localStorage['style'] = JSON.stringify(style);
    if(style.colorenabled) {
        document.getElementById('highlight-color-picker').removeAttribute('disabled');
    } else {
        document.getElementById('highlight-color-picker').setAttribute('disabled', 'disabled');
    }
    chrome.tabs.query({}, function (tabs) {
        for(var tabIdx = 0; tabIdx < tabs.length; tabIdx += 1) {
            chrome.tabs.sendMessage(tabs[tabIdx].id, {
                'name': 'setHighlightStyle',
                'style': style
            })
        }
    });

}

function save_connection_options() {
    var serverURL = document.getElementById('serverURL').value;
    //This regular expression is borrowed from the jQuery validation library found at http://jqueryvalidation.org/
    if(!/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(serverURL)) {
        document.getElementById('serverURL').style.borderColor = '#ff0000';
        document.getElementById('serverURL').style.borderWidth = 1;
        document.getElementById('serverURLError').className = "status error"
    } else {

        document.getElementById('serverURL').style.removeProperty('border-color');
        document.getElementById('serverURL').style.removeProperty('border-width');
        localStorage['serverURL'] = serverURL;
        document.getElementById('serverURLError').className = "status";
    }


}

function save_authentication_options() {
    var username = document.getElementById('username').value,
        password = document.getElementById('password').value;

    localStorage['authentication'] = JSON.stringify({username:username, password: password});
}


function restore_options() {
    var style = localStorage['style'],
        serverURL = localStorage['serverURL'],
        authentication = localStorage['authentication'];
    if (!style) {
        style = {
            'bold' : 'bold',
            'underline': 'underline',
            'colorenabled': true,
            'color': '#FF0000'
        }
    } else {
        style = JSON.parse(style);
    }

    if(!serverURL) {
        serverURL = 'http://localhost:8080';
    }

    if(!authentication) {
        authentication = {
            username: "",
            password: ""
        }
    } else {
        authentication = JSON.parse(authentication)
    }

    if(style.bold === 'bold') {
        document.getElementById('highlight-bold').setAttribute('checked', 'checked');
    }
    if(style.underline === 'underline') {
        document.getElementById('highlight-underline').setAttribute('checked', 'checked');
    }

    document.getElementById('highlight-color-picker').value = style.color;

    if(style.colorenabled) {
        document.getElementById('highlight-color').setAttribute('checked', 'checked');

    } else {
        document.getElementById('highlight-color-picker').setAttribute('disabled', 'disabled');
    }

    document.getElementById('serverURL').value = serverURL;

    document.getElementById('username').value = authentication.username;
    document.getElementById('password').value = authentication.password;

    //Assume that we are not logged in
    document.getElementById('loginBtn').style.display = "inline-block";
    document.getElementById('logoutBtn').style.display = "none";

    //If we are logged in, show the logout button
    chrome.cookies.get({url: serverURL, name: "GAS"}, function(cookie) {
        if(cookie) {
            var statusBox =  document.getElementById('loginStatus');
            document.getElementById('loginBtn').style.display = "none";
            document.getElementById('logoutBtn').style.display = "inline-block";
            statusBox.className = "status info";
            statusBox.textContent = "Logged in";
        }
    })

}

function attachLoginHandlers() {
    //Get the login and logout buttons
    var loginBtn = document.getElementById('loginBtn'),
        logoutBtn = document.getElementById('logoutBtn'),
        serverURL =  document.getElementById('serverURL').value,
        statusBox =  document.getElementById('loginStatus');

    //Handle clicking the login button
    loginBtn.onclick = function() {
        var loginData = {},
            xhr = new XMLHttpRequest();

        //Pull the login information from the page
        loginData.user = document.getElementById('username').value;
        loginData.password =  document.getElementById('password').value;

        //Set up a response handler to verify the user was successfully able to login
        xhr.onload = function() {
            //if we've successfully logged in:
            if(this.status === 200) {

                //Notify the user that we've logged in
               statusBox.textContent = "Successfully Logged In";
               statusBox.className = "status success";
                loginBtn.removeAttribute('disabled');
                document.getElementById('loginBtn').style.display = "none";
                document.getElementById('logoutBtn').style.display = "inline-block";
                chrome.runtime.getBackgroundPage(function (backgroundPage){
                    backgroundPage.setPageFiltering();
                })


            } else {
                statusBox.textContent = "Invalid Username or Password";
                document.getElementById('loginStatus').className = "status error";
                loginBtn.removeAttribute('disabled');
            }
        }

        //Add an error handler
        xhr.addEventListener("error", function() {
            //Notify the user of login failure
            statusBox.textContent = "Unable to connect to server";
            statusBox.className = "status error";
            loginBtn.removeAttribute('disabled');
        }, false);

        //Make sure to handle cookies coming back
        xhr.withCredentials = true;
        //Open a connection to the server
        xhr.open("POST",serverURL + "/user/login");

        //Notify the user that we're about to start logging in
        statusBox.className = "status info";
        statusBox.textContent = "Logging in...";
        loginBtn.setAttribute('disabled', 'disabled');

        xhr.setRequestHeader('Content-Type', 'application/json');

        //Send the form data
        xhr.send(JSON.stringify(loginData));


    }

    //Handle logging out
    logoutBtn.onclick = function() {
        var xhr = new XMLHttpRequest();

        //Set up a response handler to verify the user was successfully able to logout
        xhr.onload = function() {
            //if we've successfully logged in:
            if(this.status === 200) {

                //Notify the user that we've logged in
                statusBox.textContent = "Successfully logged out";
                statusBox.className = "status success";
                logoutBtn.removeAttribute('disabled');
                document.getElementById('loginBtn').style.display = "inline-block";
                document.getElementById('logoutBtn').style.display = "none";

            } else {
                statusBox.textContent = "Server could not log us out";
                document.getElementById('loginStatus').className = "status error";
                logoutBtn.removeAttribute('disabled');
            }
        }

        //Add an error handler
        xhr.addEventListener("error", function() {
            //Notify the user of logout failure
            statusBox.textContent = "Unable to connect to server";
            statusBox.className = "status error";
            logoutBtn.removeAttribute('disabled');
        }, false);

        //Make sure to handle cookies coming back
        xhr.withCredentials = true;
        //Open a connection to the server
        xhr.open("POST",serverURL + "/user/logout");

        //Notify the user that we're about to start logging in
        statusBox.className = "status info";
        statusBox.textContent = "Logging out...";
        logoutBtn.setAttribute('disabled', 'disabled');

        //Send the form data
        xhr.send();


    }
}


document.addEventListener('DOMContentLoaded', restore_options);
document.addEventListener('DOMContentLoaded', attachLoginHandlers);

//Attach all handlers once the DOM has been loaded
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('highlight-bold').addEventListener('change', save_style_options);
    document.getElementById('highlight-underline').addEventListener('change', save_style_options);
    document.getElementById('highlight-color').addEventListener('change', save_style_options);
    document.getElementById('highlight-color-picker').addEventListener('change', save_style_options);
    document.getElementById('serverURL').addEventListener('keyup', save_connection_options);
    document.getElementById('username').addEventListener('keyup', save_authentication_options);
    document.getElementById('password').addEventListener('keyup', save_authentication_options);
})

