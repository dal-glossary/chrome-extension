//Request information on the stylesheet information.  This must use chrome's script communication mechanism
//and so cannot be put into the shared code.

var pop, myPop;
function requestStylesheetUpdate() {
    chrome.runtime.sendMessage({name: 'getHighlightStyle'});
}
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    switch(request.name) {
    case 'setHighlightStyle':
        updateStyleSheet(request.style);
        break;
    }
});
function performGetFor(path) {

    var p = new promise.Promise();
    chrome.runtime.sendMessage({name: 'getBaseURL'}, function(url){
        promise.get(url + '/' + path + '?page_id=' + pageID ).then(function(error, result) {
            p.done(error, result);
        });
    });
    return p;
}

function displayTerm(term, refElement) {
    pop.innerHTML = "id: " + term;
    myPop.open(refElement);
}

function createPopup() {
    pop = document.createElement("div");
    document.body.appendChild(pop);
    myPop = popup(pop);

}


addHighlighting();
createPopup();