chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    switch(request.name) {
    case 'getHighlightStyle':
        var style = localStorage['style'];
        if (!style) {
            style = {
                'bold' : 'bold',
                'underline': 'underline',
                'colorenabled': true,
                'color': '#FF0000'
            }
        } else {
            style = JSON.parse(style);
        }

        chrome.tabs.query({}, function (tabs) {
            for(var tabIdx = 0; tabIdx < tabs.length; tabIdx += 1) {
                chrome.tabs.sendMessage(tabs[tabIdx].id, {
                    'name': 'setHighlightStyle',
                    'style': style
                })
            }
        });

        break;
    case 'getBaseURL':
        var serverURL = localStorage['serverURL'];
        if(!serverURL) {
            serverURL = 'http://localhost:8080'
        }
        sendResponse(serverURL);
        break;

    case 'displayTerm':
        chrome.windows.create({
            url: '../shared/termview.html#/term/' + request.term,
            type: 'panel'
        });
        break;
    }
});


chrome.runtime.onStartup.addListener(function() {
    setPageFiltering();
})

chrome.runtime.onInstalled.addListener(function() {
    chrome.tabs.create({
        url: "../shared/welcome.html"
    })
})