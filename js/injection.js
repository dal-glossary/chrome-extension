/**
 * This function is a callback for onDOMContentLoaded
 * @param details The details object for divining further information about the page where content was loaded
 */
function injectHighlighting(details, pageID) {
    chrome.tabs.executeScript(details.tabId,  {
        file: "shared/highlight.lib.js"
    });
    chrome.tabs.executeScript(details.tabId,  {
        file: "shared/highlight.min.js"
    });
    chrome.tabs.executeScript(details.tabId,  {
        file: "shared/injection.min.js"
    });
    chrome.tabs.executeScript(details.tabId, {
        code: "var pageID = " + pageID + ";"
    });
    chrome.tabs.executeScript(details.tabId,  {
        file: "js/runOnLoad.js"
    });
}

function setPageFiltering() {

    function updateFilters() {
        var pages = localStorage['pages'],
            filters = [],
            URLRegexes = [];
        if(!pages) {
            pages = [];
        } else {
            pages = JSON.parse(pages);
        }

        for(var i = 0; i < pages.length; i += 1) {
            if(pages[i].name) {
                filters.push({
                    urlMatches: pages[i].url
                });
                URLRegexes.push(new RegExp(pages[i].url))
            }
        }
        var onContentLoaded = function(details) {
            var pageID;
            for(var i = 0; i < URLRegexes.length; i += 1) {
                if(details.url.match(URLRegexes[i])) {
                    pageID = pages[i].id;
                    break;
                }
            }
            injectHighlighting(details, pageID);
        }
        chrome.webNavigation.onDOMContentLoaded.removeListener(onContentLoaded);
        chrome.webNavigation.onDOMContentLoaded.addListener(onContentLoaded, {url: filters});

    }

    //In case the request fails, in order to save time, set up the filters now
    updateFilters();
    //Make a request
    var xhr = new XMLHttpRequest(),
        serverURL = localStorage['serverURL'];
    if(!serverURL) {
        serverURL = "http://localhost:8080"
    }
    xhr.onload = function() {
        if(this.status === 200) {
            localStorage['pages'] = xhr.responseText;
            updateFilters();
        }
    };

    xhr.open("GET", serverURL + "/page");
    xhr.send();


}

setPageFiltering();