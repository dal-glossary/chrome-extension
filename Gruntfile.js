module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            all: {
                files: [
                    {
                        expand: true,
                        src: ['shared-extension/build/*'],
                        dest: 'deploy/shared/',
                        flatten: true,
                        filter:  function(file) {
                            return (grunt.option('dev')? true :  !/\.map$/.test(file)) && !/\.coffee\./.test(file) ;
                        }

                    },
                    {
                        expand: true,
                        src: ['js/*.js'],
                        dest: 'deploy/js',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['css/*.css'],
                        dest: 'deploy/css',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['html/*.html'],
                        dest: 'deploy/html',
                        flatten: true
                    },
                    {
                        src: 'manifest.json',
                        dest: 'deploy/manifest.json'
                    }
                ],
                options: {
                    processContent: grunt.option('no-dev')? undefined: function(content, srcPath) {
                        if(/\.map$/.test(srcPath)) {
                            var mapFile = JSON.parse(content);
                            //chop off the ../ at the beginning of the source root;
                            mapFile.sourceRoot = "../../shared-extension/" + mapFile.sourceRoot.substring(3)
                            return JSON.stringify(mapFile);
                        } else {
                            return content;

                        }
                }
                }

            }
        },

        replace: {
            welcome: {
                src: ['deploy/shared/welcome.html'],
                overwrite: true,
                replacements: [{
                    from: 'chrome://path/to/options/dialog',
                    to: 'chrome-extension://jjdnablelhmjenhhbglfnpiibhiljpll/html/options.html'
                }]
            }
        },

        shell: {
            buildShared: grunt.option('no-build-shared') ? {command: ''} : {
                command: 'npm install && grunt build' + (grunt.option('dev')? ' --dev' : ''),

                options: {
                    failOnError: true,
                    stdout: true,
                    stderr: true,
                    execOptions: {
                        cwd: 'shared-extension'
                    }

                }
            }
        },

        clean:  ['deploy', 'shared-extension/build']

    }
    );


    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-text-replace');


    grunt.registerTask('build', ['shell:buildShared', 'copy:all', 'replace']);



    // Default task(s).
    grunt.registerTask('default', ['build']);


};